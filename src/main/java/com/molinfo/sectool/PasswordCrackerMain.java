package com.molinfo.sectool;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@RestController
public class PasswordCrackerMain {

    public static LocalTime startTime;
    public static LocalTime endTime;


    @GetMapping("/compute-password")
    public Map<String, Object> computePassword(@RequestParam(value = "number-of-threads") int numThreads, @RequestParam(value = "pw-length") int passwordLength,
                                               @RequestParam(value = "hash") String encryptedPassword, @RequestParam(value = "termination-crit") boolean isEarlyTermination, @RequestParam(value = "alg" ) String algorithm) {
        Map<String, Object> response = new HashMap<>();

        startTime = LocalTime.now();
        System.out.println("called + hash "+encryptedPassword+" number threads "+ numThreads+" maxLength "+ passwordLength);
        ExecutorService workerPool = Executors.newFixedThreadPool(numThreads);
        PasswordFuture passwordFuture = new PasswordFuture();
        PasswordCrackerConsts passwordCrackerConsts = new PasswordCrackerConsts(numThreads, passwordLength, encryptedPassword, algorithm);

        /*
         * Create PasswordCrackerTask and use executor service to run in a separate thread
         */
        for (int i = 0; i < numThreads; i++) {
            workerPool.submit(new PasswordCrackerTask(i, isEarlyTermination, passwordCrackerConsts, passwordFuture));
        }

        String password = null;
        try {
            password = passwordFuture.get();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            workerPool.shutdown();
        }

        endTime = LocalTime.now();
        response.put("password", password);
        response.put("duration", (Duration.between(startTime, endTime).toMillis() / 1000 + " secondi").toString());
        response.put("code", (200));

        return response;
    }
}

/**
 * A {@code Future} represents the result of an asynchronous
 * computation.  Methods are provided to check if the computation is
 * complete, to wait for its completion, and to retrieve the result of
 * the computation.  The result can only be retrieved using method
 * {@code get} when the computation has completed, blocking if
 * necessary until it is ready.  Cancellation is performed by the
 * {@code cancel} method.  Additional methods are provided to
 * determine if the task completed normally or was cancelled. Once a
 * computation has completed, the computation cannot be cancelled.
 * If you would like to use a {@code Future} for the sake
 * of cancellability but not provide a usable result, you can
 * declare types of the form {@code Future<?>} and
 * return {@code null} as a result of the underlying task.
 **/
// Per accedere alla documentazione sulla classe Future, seguire il link:
// https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Future.html


class PasswordFuture implements Future<String> {
    String result;
    Lock lock = new ReentrantLock();
    Condition resultSet = lock.newCondition();

    /*  ### set ###
     *setta result e manda il signal al thread che aspetta il result
     */
    public void set(String result) {
        lock.lock();
        this.result = result;
        resultSet.signal();
        lock.unlock();
    }

    /*  ### get ###
     *  se il result è pronto viene ritornato.
     *  altrimenti aspetta.
     */
    @Override
    public String get() throws InterruptedException {
        lock.lock();
        try {
            while (!isDone()) {

                resultSet.await();
            }
        } finally {
            lock.unlock();
        }

        return result;
    }

    /*  ### isDone ###
     * torna true se result è stato settato
     */
    @Override
    public boolean isDone() {

        return (result != null);
    }


    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public String get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return null;
    }
}
