package com.molinfo.sectool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SectoolApplication {

    public static void main(String[] args) {
        SpringApplication.run(SectoolApplication.class, args);
    }

}
